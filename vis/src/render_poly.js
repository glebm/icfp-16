const NEGATIVE_POLYGON_OPTIONS = {
  fillColor: 'red',
  fillOpacity: 0.8,
  highlightFillColor: 'red',
  highlightFillOpacity: 0.5,
  strokeColor: 'red',
};

function renderPolyFromPoints(board, points, polygonName = '', positive = calcSignedArea(points) >= 0) {
  const opts = positive ? {} : NEGATIVE_POLYGON_OPTIONS;
  return board.create('polygon', points, { name: polygonName, hasInnerPoints: true, withLabel: true, ...opts });
}

const renderPoly = (board, poly, polygonName = 'p') => {
  let i = 0;
  const points = poly.map(([x, y]) => {
    const name = `${polygonName}_{${++i}}`;
    return board.create('point', [x, y], { name: name, size: 2 })
  });
  renderPolyFromPoints(board, points, polygonName);
};

function calcSignedArea(points) {
  let area = 0;
  const n = points.length;
  for (let i = 0; i < n; i++) {
    area += (points[(i + 1) % n].X() - points[i].X()) * (points[(i + 1) % n].Y() + points[i].Y())
  }
  return -area / 2;
}


export default renderPoly;
export {renderPolyFromPoints};
