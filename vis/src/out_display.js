import React, {Component} from 'react';
import {renderPolyFromPoints} from './render_poly';
import {parseOutput} from './parse';

const JXG = window.JXG;

const BOARD_OPTS = {
  axis: true,
  grid: true,
  keepaspectratio: true,
  showCopyright: false,
  pan: {
    enabled: true,
    needTwoFingers: false,
    needshift: false,
  },
  zoom: {
    wheel: true,
  }
};

const DEFAULT_DATA = `
7
0,0
1,0
1,1
0,1
0,1/2
1/2,1/2
1/2,1
4
4 0 1 5 4
4 1 2 6 5
3 4 5 3
3 5 6 3
0,0
1,0
0,0
0,0
0,1/2
1/2,1/2
0,1/2
`.trim();

class OutDisplay extends Component {

  constructor(props) {
    super(props);
    this.state = { src: DEFAULT_DATA };
  }


  handleSrcChange(event) {
    this.setState({ src: event.target.value });
  }

  render() {
    this.data = parseOutput(this.state.src.trim());
    return (
        <div>
          <h2>Output</h2>
          <textarea className="App-textarea" value={this.state.src}
                    onChange={this.handleSrcChange.bind(this)}/>
          <div style={{ display: 'inline-block', margin: '0.5rem 1rem', verticalAlign: 'top' }}>
            <div id="box1" style={{ width: '600px', height: '600px' }}/>
          </div>
          <div style={{ display: 'inline-block', margin: '0.5rem 1rem', verticalAlign: 'top' }}>
            <div id="box2" style={{ width: '600px', height: '600px' }}/>
            <button type="button" style={{ marginTop: '0.5rem' }}
                    onClick={this.setViewBoxToBoundingBoxOfData.bind(this)}>Fit bounding box
            </button>
          </div>
        </div>
    )
  }

  componentDidMount() {
    this.jsxRender();
  }

  componentDidUpdate() {
    this.jsxRender();
  }

  setViewBoxToBoundingBoxOfData() {
    const points = this.data.destinationPositions;
    const bb = [
      Math.min(...points.map(e => e[0])),
      Math.max(...points.map(e => e[1])),
      Math.max(...points.map(e => e[0])),
      Math.min(...points.map(e => e[1])),
    ];
    console.log('Setting bounding box to', bb);
    this.destBoard.setBoundingBox(bb, true);
  }

  jsxRender() {
    const srcBoard = JXG.JSXGraph.initBoard('box1', {boundingbox: [-0.1, 1.1, 1.1, -0.1], ...BOARD_OPTS});
    const destBoard = this.destBoard = JXG.JSXGraph.initBoard('box2', {boundingbox: [-0.1, 1.1, 1.1, -0.1], ...BOARD_OPTS});

    let i = 0;
    const srcPoints = this.data.sourcePositions.map((p) => srcBoard.create('point', p, {name: `${++i}`, size: 2}));
    i = 0;
    const dstPoints = this.data.destinationPositions.map((p) => destBoard.create('point', p, {name: `${++i}`, size: 2}));

    i = 0;
    this.data.facets.forEach((poly) => {
      const name = `P_{${i}}`;
      renderPolyFromPoints(srcBoard, poly.map((i) => srcPoints[i]), name, true);
      renderPolyFromPoints(destBoard, poly.map((i) => dstPoints[i]), name, true);
      i++;
    });
  }


}

export default OutDisplay;
