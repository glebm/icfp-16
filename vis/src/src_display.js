import React, {Component} from 'react';
import renderPoly from './render_poly';
import {parseInput} from './parse';

const JXG = window.JXG;

const SKEL_POINT_OPTS = {
  size: 2,
  label: {
    offset: [2, -6],
    anchorX: 'middle',
    anchorY: 'top'
  }
};

const DEFAULT_DATA = `
2
11
11/25,3/25
1/2,1/5
14/25,3/25
16/25,9/50
9/16,17/60
13/20,2/5
3/5,1/2
2/5,1/2
7/20,2/5
7/16,17/60
9/25,9/50
3
1/2,11/30
19/40,2/5
21/40,2/5
10
14/25,3/25 16/25,9/50
11/25,3/25 13/20,2/5
9/25,9/50 3/5,1/2
7/20,2/5 2/5,1/2
11/25,3/25 9/25,9/50
14/25,3/25 7/20,2/5
13/20,2/5 3/5,1/2
16/25,9/50 2/5,1/2
7/20,2/5 13/20,2/5
2/5,1/2 3/5,1/2
`.trim();

class SrcDisplay extends Component {

  constructor(props) {
    super(props);
    this.state = { src: DEFAULT_DATA };
  }


  handleSrcChange(event) {
    this.setState({ src: event.target.value });
  }

  render() {
    this.data = parseInput(this.state.src.trim());
    return (
        <div>
          <h2>Input</h2>
          <textarea className="App-textarea" value={this.state.src}
                    onChange={this.handleSrcChange.bind(this)}/>
          <div style={{ display: 'inline-block', margin: '0.5rem 1rem' }}>
            <div id="box" style={{ width: '600px', height: '600px' }}/>
            <button type="button" style={{ marginTop: '0.5rem' }} onClick={this.setViewBoxToBoundingBoxOfData.bind(this)}>Fit bounding box</button>
          </div>
        </div>
    )
  }

  componentDidMount() {
    this.jsxRender();
  }

  componentDidUpdate() {
    this.jsxRender();
  }

  setViewBoxToBoundingBoxOfData() {
    const points = Array.prototype.concat([], ...this.data.sil);
    const bb = [
      Math.min(...points.map(e => e[0])),
      Math.max(...points.map(e => e[1])),
      Math.max(...points.map(e => e[0])),
      Math.min(...points.map(e => e[1])),
    ];
    console.log('Setting bounding box to', bb);
    this.board.setBoundingBox(bb, true);
  }

  jsxRender() {
    const b = this.board = JXG.JSXGraph.initBoard('box', {
      axis: true,
      boundingbox: [-0.2, 1.2, 1.2, -0.2],
      grid: true,
      keepaspectratio: true,
      showCopyright: false,
      pan: {
        enabled: true,
        needTwoFingers: false,
        needshift: false,
      },
      zoom: {
        wheel: true,
      }
    });

    let i = 0;
    this.data.skel.forEach(([xy1, xy2]) => {
      const name = `s_{${++i}}`;
      const p1 = b.create('point', xy1, { name: name, face: 'o', ...SKEL_POINT_OPTS });
      const p2 = b.create('point', xy2, { name: name, face: 'x', ...SKEL_POINT_OPTS });
      b.create('segment', [p1, p2], { dash: 2 });
    });

    i = 0;
    this.data.sil.forEach((poly) => renderPoly(b, poly, `${String.fromCharCode(97 + i++).toUpperCase()}`));
  }


}

export default SrcDisplay;
