const toI = (e) => parseInt(e, 10);

const parseRational = (text) => {
  const nums = text.split('/').map(toI);
  return (nums.length === 2) ? nums[0] / nums[1] : nums[0];
};
const parseVertex = (line) => line.trim().split(',').map(parseRational);

function parseInput(src) {
  let lines = src.trim().split("\n");
  const numPolys = toI(lines.shift());
  const polys = [];
  for (let i = 0; i < numPolys; i++) {
    let numPoints = toI(lines.shift());
    polys.push(lines.slice(0, numPoints).map(parseVertex));
    lines = lines.slice(numPoints);
  }
  lines.shift();
  const skel = lines.map((line) => line.trim().split(' ').map(parseVertex));
  return { sil: polys, skel: skel };
}

function parseOutput(src) {
  let lines = src.trim().split("\n");
  const sourcePositions = lines.slice(0, toI(lines.shift())).map(parseVertex);
  lines = lines.slice(sourcePositions.length);
  const numFacets = toI(lines.shift());
  const facets = [];
  for (let i = 0; i< numFacets; i++) {
    // eslint-disable-next-line
    let [_, ...vertexIndices] = lines[i].split(" ").map(toI);
    facets.push(vertexIndices);
  }
  const destinationPositions = lines.slice(numFacets).map(parseVertex);
  return { sourcePositions, facets, destinationPositions };
}

export {parseInput, parseOutput};
