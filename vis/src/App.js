import React, {Component} from 'react';
import './App.css';
import SrcDisplay from './src_display';
import OutDisplay from './out_display';



class App extends Component {
  render() {
    return (
        <div className="App">
          <h1>ICFP 2016 Problem Visualiser</h1>
          <a href="http://icfpc2016.blogspot.co.uk/2016/08/task-description.html">task description</a> (<a href="https://drive.google.com/file/d/0BwQpNipA7igIa0lhUjEwV3FBOTg/view?usp=sharing">pdf</a>)
          | <a href="http://2016sv.icfpcontest.org/problem/list">problems</a>
          <SrcDisplay/>
          <OutDisplay/>
        </div>
    );
  }
}

export default App;
