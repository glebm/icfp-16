module Geometry where

import Prelude hiding (subtract)
import Data.Ord
import Data.Maybe
import Data.Map (Map)
import Data.Set (Set)
import Data.Ratio
import Data.Foldable
import qualified Data.Map as Map
import qualified Data.Set as Set

import Types

-- TODO: implement
quickHull :: [Point] -> Set Point
quickHull [] = Set.empty
quickHull ls =
  let a = minimumBy (comparing fst) ls
      b = maximumBy (comparing fst) ls
      splitted = buildMultimap (\x -> leftTurn a b x) ls
      left = fromMaybe [] (Map.lookup LT splitted)
      right = fromMaybe [] (Map.lookup GT splitted)
  in mconcat [ quickHullStep a b left
             , quickHullStep a b right
             , Set.fromList [ a, b ]
             ]

subtract :: Point -> Point -> Point
subtract (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

crossProduct :: Point -> Point -> Rational
crossProduct (x1, y1) (x2, y2) = x1 * y2 - x2 * y1

directedArea :: Point -> Point -> Point -> Rational
directedArea x y z = (x `subtract` z) `crossProduct` (y `subtract` z)

leftTurn :: Point -> Point -> Point -> Ordering
leftTurn x y z = directedArea x y z `compare` 0

buildMultimap :: Ord b => (a -> b) -> [a] -> Map b [a]
buildMultimap f = foldr (\x -> Map.insertWith (++) (f x) [x]) Map.empty

insideTriangle :: Point -> Point -> Point -> Point -> Bool
insideTriangle x y z c =
  -- `c` stands for `candidate`, x, y and z define the triangle
  and [ leftTurn x y z == leftTurn x y c
      , leftTurn y z x == leftTurn y z c
      , leftTurn z x y == leftTurn z x c
      ]

quickHullStep :: Point -> Point -> [Point] -> Set Point
quickHullStep a b [] = Set.empty
quickHullStep a b ls =
  let c = maximumBy (comparing (abs . directedArea a b)) ls
      remainder = filter (not . insideTriangle a b c) ls
      acb = leftTurn a c b
      splitted = buildMultimap (\x -> leftTurn a c x == acb) remainder
      left = quickHullStep a c (fromMaybe [] (Map.lookup False splitted))
      right = quickHullStep c b (fromMaybe [] (Map.lookup True splitted))
  in mconcat [ left, Set.singleton c, right ]

testCase :: [Point]
testCase =
  [ (0, 0)
  , (0, 1)
  , (1, 0)
  , (1, 1)
  , (1 % 2, 1 % 3)
  ]
