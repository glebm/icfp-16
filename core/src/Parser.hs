module Parser where

import Control.Applicative ((<$>), (<*>), (*>), (<*))
import Control.Monad
import Data.Ratio
import Text.Parsec.Char
import Text.Parsec.Prim
import Text.Parsec.String
import Text.Parsec.Combinator

import Types

integer :: Read a => Parser a
integer = read <$> many1 (oneOf (['0'..'9']++['-']))

rational :: Parser Rational
rational = (%) <$> integer <*> ((char '/' *> integer) <|> return 1)

point :: Parser Point
point = (,) <$> (rational <* char ',') <*> rational

edge :: Parser Edge
edge = Edge <$> (point <* spaces) <*> point

edges :: Parser [Edge]
edges =
  do n <- integer
     replicateM n (spaces *> edge)

polygon :: Parser Polygon
polygon =
  do n <- integer
     replicateM n (spaces *> point)

problem :: Parser Problem
problem =
  do n <- integer
     polygons <- replicateM n (spaces *> polygon)
     m <- spaces *> integer
     edges <- replicateM m (spaces *> edge)
     return (Problem polygons edges)
