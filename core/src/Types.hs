module Types where

import Data.Ratio

type Point = (Rational, Rational)
type Polygon = [Point]

data Edge = Edge
  { edgeStart :: Point
  , edgeEnd :: Point
  }
  deriving (Show)

data Problem = Problem
  { problemPolygons :: [Polygon]
  , problemEdges :: [Edge]
  }
  deriving (Show)
