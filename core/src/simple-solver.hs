import Parser (problem)
import Types

import Data.Array
import Data.IORef
import Data.List
import Data.Ratio
import System.Random
import System.Environment
import Text.Parsec.String

data Vertex = Vertex {
	x :: Rational,
	y :: Rational,
	idx :: Int
} deriving (Show)

v idx x y = Vertex { x = x, y = y, idx = idx }

data Facet = Facet { -- convex
	vertices :: Array Int Vertex
} deriving (Show)

formatRational r =
		if d == 1
			then (show n)
			else (show $ n) ++ "/" ++ (show $ d)
	where
		n = numerator r
		d = denominator r

formatVertex v = (formatRational $ x $ v) ++ "," ++ (formatRational $ y $ v)

mkRnd low high = do
	above <- getStdRandom (randomR (10 :: Integer, 100000))
	below <- getStdRandom (randomR (10, 100000))
	let frac = above % below
	if (low < frac) && (frac < high)
		then return frac
		else mkRnd low high

flipTranslatePoint line_a ox oy vert = v vi (xx + ox) (yy + oy)
	where
		a = line_a
		d = (vx + vy * a) / (1 + a * a)
		xx = 2 * d - vx
		yy = 2 * d * a - vy
		vx = x vert
		vy = y vert
		vi = idx vert

mkOff = do
	scale <- getStdRandom (randomR (10 :: Integer, 100000))
	r <- mkRnd 0 1
	let r2 = r - (1 % 2)
	return $ (scale % 1) * r2

transformPoints verts = do
	liney <- mkRnd 0 1
	offsetx <- mkOff
	offsety <- mkOff
	let line_a = (1 - liney) / liney
--	putStrLn $ "line_a: " ++ (show line_a)
	let outverts = map (flipTranslatePoint line_a offsetx offsety) verts
	return outverts

getBounds pdesc = ((minimum xs, maximum xs), (minimum ys, maximum ys))
	where
		points = concat [[edgeStart e, edgeEnd e] | e <- problemEdges pdesc]
		xs = map fst points
		ys = map snd points

-- 0-----1--4
-- |     |  |
-- |     |  |
-- 3-----2--7
-- |     |  |
-- 6-----8--5

main = do
	args <- getArgs
	let fname = args !! 0
--	putStrLn $ fname
	Right pdesc <- parseFromFile problem fname
	let ((offx, maxx), (offy, maxy)) = getBounds pdesc
	let (bottom, right) = (min 1 $ maxx - offx, min 1 $ maxy - offy)

	let startverts = [
		v 0 0 0,
		v 4 1 0,
		v 5 1 1,
		v 6 0 1,
		v 1 right 0,
		v 2 right bottom,
		v 3 0 bottom,
		v 7 1 bottom,
		v 8 right 1]
	let transformed = [
		v 0 (offx+0) (offy+0),
		v 4 (offx + 2*right - 1) (offy+0),
		v 5 (offx + 2*right - 1) (offy + 2*bottom - 1),
		v 6 (offx+0) (offy + 2*bottom - 1),
		v 1 (offx+right) (offy+0),
		v 2 (offx+right) (offy+bottom),
		v 3 (offx+0) (offy+bottom),
		v 7 (offx + 2*right - 1) (offy+bottom),
		v 8 (offx+right) (offy + 2*bottom - 1)]
	let getpos verts want_idx =
			it
		where
			Just it = find (\foo -> idx foo == want_idx) verts
	let subsetit poslist = (st, tr)
		where
			st = map (getpos startverts) poslist
			tr = map (getpos transformed) poslist
--	let outverts = map getpos [11, 8, 9, 10, 4, 5, 6, 7, 12, 13, 14, 15]

--	putStrLn $ show (bottom, right)

	case (bottom, right) of
		(1, 1) -> do
			let (st, tr) = subsetit [0, 1, 2, 3]
			putStrLn $ (show $ length $ st)
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) st
			putStrLn "1"
			putStrLn "4 0 1 2 3"
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) tr
		(_, 1) -> do
			let (st, tr) = subsetit [0, 1, 2, 3, 6, 8]
			putStrLn $ (show $ length $ st)
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) st
			putStrLn "2"
			putStrLn "4 0 1 2 3"
			putStrLn "4 2 5 4 3"
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) tr
		(1, _) -> do
			let (st, tr) = subsetit [0, 1, 2, 3, 4, 7]
			putStrLn $ (show $ length $ st)
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) st
			putStrLn "2"
			putStrLn "4 0 1 2 3"
			putStrLn "4 1 4 5 2"
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) tr
		(_, _) -> do
			let (st, tr) = subsetit [0, 1, 2, 3, 4, 5, 6, 7, 8]
			putStrLn $ (show $ length $ st)
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) st
			putStrLn "4"
			putStrLn "4 0 1 2 3"
			putStrLn "4 1 4 7 2"
			putStrLn "4 2 7 5 8"
			putStrLn "4 2 8 6 3"
			mapM_ (\foo -> putStrLn $ formatVertex $ foo) tr
	return ()
