module Main where

import Text.Parsec.String

import Parser (problem)

main :: IO ()
main = print =<< parseFromFile problem "input.txt"
