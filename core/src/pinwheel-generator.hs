import Data.Array
import Data.IORef
import Data.List
import Data.Ratio
import System.Random

data Vertex = Vertex {
	x :: Rational,
	y :: Rational,
	idx :: Int
} deriving (Show)

v idx x y = Vertex { x = x, y = y, idx = idx }

data Facet = Facet { -- convex
	vertices :: Array Int Vertex
} deriving (Show)

formatRational r =
		if d == 1
			then (show n)
			else (show $ n) ++ "/" ++ (show $ d)
	where
		n = numerator r
		d = denominator r

formatVertex v = (formatRational $ x $ v) ++ "," ++ (formatRational $ y $ v)

mkRnd low high = do
	above <- getStdRandom (randomR (10 :: Integer, 100000))
	below <- getStdRandom (randomR (10, 100000))
	let frac = above % below
	if (low < frac) && (frac < high)
		then return frac
		else mkRnd low high

flipTranslatePoint line_a ox oy vert = v vi (xx + ox) (yy + oy)
	where
		a = line_a
		d = (vx + vy * a) / (1 + a * a)
		xx = 2 * d - vx
		yy = 2 * d * a - vy
		vx = x vert
		vy = y vert
		vi = idx vert

mkOff = do
	scale <- getStdRandom (randomR (10 :: Integer, 100000))
	r <- mkRnd 0 1
	let r2 = r - (1 % 2)
	return $ (scale % 1) * r2

transformPoints verts = do
	liney <- mkRnd 0 1
	offsetx <- mkOff
	offsety <- mkOff
	let line_a = (1 - liney) / liney
--	putStrLn $ "line_a: " ++ (show line_a)
	let outverts = map (flipTranslatePoint line_a offsetx offsety) verts
	return outverts

main = do
--	x1 <- mkRnd 0 (1 % 2)
--	x2 <- mkRnd (1 % 2) 1
--	y1 <- mkRnd 0 (1 % 2)
--	y2 <- mkRnd (1 % 2) 1
	let x1 = 1 % 4
	let x2 = 3 % 4
	let y1 = 1 % 4
	let y2 = 3 % 4
	let xtr = (1 - x2) * 2
	let xbl = (x1 - 0) * 2
	let ytl = (y1 - 0) * 2
	let ybr = (1 - y2) * 2
	let middle = 1 % 2
	let sheet = [v 0 0 0, v 1 1 0, v 2 1 1, v 3 0 1]
	let square = [v 4 x1 y1, v 5 x2 y1, v 6 x2 y2, v 7 x1 y2]
	let others = [v 8 xtr 0, v 9 1 ybr, v 10 xbl 1, v 11 0 ytl]
	let startverts = sheet ++ square ++ others
	let trg_12_from_8 = v 12 xtr (y1 * 2)
	let trg_13_from_9 = v 13 ((1 - x2) * 2) ybr
	let trg_14_from_10 = v 14 xbl ((1 - y2) * 2)
	let trg_15_from_11 = v 15 (x1 * 2) ytl
	let allverts = startverts ++ [trg_12_from_8, trg_13_from_9, trg_14_from_10, trg_15_from_11]
	let getpos want_idx =
			it
		where
			Just it = find (\foo -> idx foo == want_idx) allverts
	let outverts = map getpos [11, 8, 9, 10, 4, 5, 6, 7, 12, 13, 14, 15]
	transformed <- transformPoints outverts

	putStrLn $ (show $ length $ startverts)
	mapM_ (\foo -> putStrLn $ formatVertex $ foo) startverts
	putStrLn "9"
	putStrLn "4 4 5 6 7"
	putStrLn "4 0 8 5 4"
	putStrLn "3 8 1 5"
	putStrLn "4 1 9 6 5"
	putStrLn "3 9 2 6"
	putStrLn "4 2 10 7 6"
	putStrLn "3 10 3 7"
	putStrLn "4 3 11 4 7"
	putStrLn "3 11 0 4"
	mapM_ (\foo -> putStrLn $ formatVertex $ foo) transformed
	return ()
