import Data.Array
import Data.IORef
import Data.Ratio
import System.Random
import System.IO.Unsafe

data Vertex = Vertex {
	x :: Rational,
	y :: Rational,
	idx :: Int
} deriving (Show)

v idx x y = Vertex { x = x, y = y, idx = idx }

data Facet = Facet { -- convex
	vertices :: Array Int Vertex
} deriving (Show)

data Shape = Simple Facet | Combination Shape Shape
	deriving (Show)

--foldFacet facet line@(v1, v2) = ...

idx_var = unsafePerformIO $ newIORef 4

plusModBound (low, high) i = if i == high then low else i + 1

vertsForEdge verts idx =
		(verts ! i1, verts ! i2)
	where
		i1 = idx
		i2 = plusModBound (bounds verts) idx

getNextIdx = do
	next <- readIORef idx_var
	writeIORef idx_var (next + 1)
	return next

chooseRandomPoint edge@(v1, v2) = do
	below <- getStdRandom (randomR (10, 200))
	above <- getStdRandom (randomR (1, below - 1))
	let frac = above % below
	let x1 = x v1
	let y1 = y v1
	let x2 = x v2
	let y2 = y v2
	let new_x = x1 + (x2 - x1) * frac
	let new_y = y1 + (y2 - y1) * frac
--	putStrLn $ (show frac) ++ " between " ++ (show v1) ++ " and " ++ (show v2)
--	putStrLn $ "  x: " ++ (show new_x)
--	putStrLn $ "  y: " ++ (show new_y)
	new_idx <- getNextIdx
	return Vertex { x = new_x, y = new_y, idx = new_idx }

fromToIncluding :: (Int, Int) -> Int -> Int -> [Int]
fromToIncluding b@(low, high) start end
	| (start == end) = [start]
	| (start == high) = [high] ++ (fromToIncluding b low end)
	| otherwise = [start] ++ (fromToIncluding b (start + 1) end)

simple lst = Simple $ Facet { vertices = listArray (1, length lst) lst }

chooseRandomFoldingLine facet = do
	let verts = vertices facet
	let b = bounds verts
	idx1 <- getStdRandom (randomR b)
	idx2 <- getStdRandom (randomR b)
	let edge1 = vertsForEdge verts idx1
	let edge2 = vertsForEdge verts idx2
	if idx1 >= idx2
		then chooseRandomFoldingLine facet
		else do
--			putStrLn $ "chose " ++ show (idx1, idx2)
--			putStrLn $ "chose " ++ show (edge1, edge2)
			pt1 <- chooseRandomPoint edge1
			pt2 <- chooseRandomPoint edge2
--			putStrLn $ "chose pt1: " ++ show pt1
--			putStrLn $ "chose pt2: " ++ show pt2
			let one = [pt1] ++ [verts ! x | x <- fromToIncluding b (plusModBound b idx1) idx2] ++ [pt2]
			let two = [pt2] ++ [verts ! x | x <- fromToIncluding b (plusModBound b idx2) idx1] ++ [pt1]
--			putStrLn $ "facet1: " ++ show one
--			putStrLn $ "facet2: " ++ show two
			return $ Combination (simple one) (simple two)

mutate shape = case shape of
	Simple facet -> do
		chooseRandomFoldingLine facet
	Combination a b -> do
		which <- getStdRandom (randomR (1 , 2)) :: IO Int
		if which == 1
			then do
				new_a <- mutate a
				return $ Combination new_a b
			else do
				new_b <- mutate b
				return $ Combination a new_b

main = do
	let sheet = simple [v 0 0 0, v 1 1 0, v 2 1 1, v 3 0 1]
	sheet2 <- mutate sheet
	putStrLn $ show sheet
	putStrLn $ show sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	sheet2 <- mutate sheet2
	putStrLn $ show sheet2
