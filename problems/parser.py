import json
import os
import time

API_KEY = '277-c24250f5d072bea49d518fe4280bc6d6'

f = open("contest-snapshot.json")

j = json.loads(f.read())

print len(j['problems'])

for p in j['problems'][:2800]:
	problem_id = p['problem_id']
	problem_spec_hash = p['problem_spec_hash']
	filename = 'downloaded_problems/%d.txt' % problem_id
	if os.path.isfile(filename): continue
	print problem_id, problem_spec_hash
	cmd = "curl --compressed -L -H Expect: -H 'X-API-Key: %s' 'http://2016sv.icfpcontest.org/api/blob/%s' > %s" % (API_KEY, problem_spec_hash, filename)
	print cmd
	os.system(cmd)
	print
	time.sleep(3.8)
