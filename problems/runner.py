import json
import os
import time

f = open("contest-snapshot.json")

j = json.loads(f.read())

print len(j['problems'])

for p in j['problems'][:2800]:
	problem_id = p['problem_id']
	problem_spec_hash = p['problem_spec_hash']
	filename = 'downloaded_problems/%d.txt' % problem_id
	dstfilename = 'generated_solutions/%d.txt' % problem_id
	if not os.path.isfile(filename): continue
	if os.path.isfile(dstfilename): continue
	print problem_id, problem_spec_hash
	cmd = 'cd ../core/src && runghc simple-solver.hs ../../problems/%s > ../../problems/%s' % (filename, dstfilename)
	print cmd
	os.system(cmd)
	print
	time.sleep(0.3)
