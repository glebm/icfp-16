import json
import os
import time

API_KEY = '277-c24250f5d072bea49d518fe4280bc6d6'

f = open("contest-snapshot.json")

j = json.loads(f.read())

print len(j['problems'])

for p in j['problems'][:500]:
	problem_id = p['problem_id']
	problem_spec_hash = p['problem_spec_hash']
	filename = 'generated_solutions/%d.txt' % problem_id
	resultfilename = 'submission_results/%d.txt' % problem_id
	if not os.path.isfile(filename): continue
	if os.path.isfile(resultfilename): continue
	print problem_id, problem_spec_hash
	cmd = "curl --compressed -L -H Expect: -H 'X-API-Key: %s' -F 'problem_id=%s' -F 'solution_spec=@%s' 'http://2016sv.icfpcontest.org/api/solution/submit' > %s" % (API_KEY, problem_id, filename, resultfilename)
	print cmd
	os.system(cmd)
	print
	time.sleep(3.8)
